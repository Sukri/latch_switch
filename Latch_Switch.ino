const int  buttonPin = 2;    // the pin that the pushbutton is attached to
const int ledPin = 15;       // the pin that the LED is attached to

int buttonPushCounter = 0;
int buttonState = HIGH;
int lastButtonState = HIGH;
int lastState = HIGH;

void setup() {
  // put your setup code here, to run once:
  pinMode(buttonPin, INPUT);
  // initialize the LED as an output:
  pinMode(ledPin, OUTPUT);
  // initialize serial communication:
  Serial.begin(9600);
}

int negate(int val) {
  return val == HIGH ? LOW : HIGH;
//  if (val == HIGH) {
//    return LOW;
//  } else if (val == LOW) {
//    return HIGH;
//  }
}

int getState(int val) {
  return val == HIGH ? 0 : 1;
}

void loop() {
  // put your main code here, to run repeatedly:

 buttonState = digitalRead(buttonPin);

  Serial.println("state:");
  Serial.println(getState(buttonState));
  Serial.println(getState(lastButtonState));


 if (buttonState == LOW && lastButtonState == HIGH) {
  Serial.println("hi");
  
   lastState = negate(lastState);
 }

  if (buttonState != lastButtonState) {
  lastButtonState = buttonState;
 }

 lastState == HIGH ? digitalWrite(ledPin, LOW) : digitalWrite(ledPin, HIGH);

  if (lastState == HIGH)
    digitalWrite(ledPin, LOW);
  else {
    digitalWrite(ledPin, HIGH);
    digitalWrite(ledPin2, HIGH);
  }

  delay(300);
}
